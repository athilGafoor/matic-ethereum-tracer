from django.apps import AppConfig


class EthCoreConfig(AppConfig):
    name = 'eth_core'
