from django.shortcuts import render
from web3 import Web3
import time
import sys
import logging
from django.db.models import Max
from django.http import JsonResponse
from .models import *
from django.db.models import Q
# Create your views here.


web3 = Web3(Web3.HTTPProvider("https://kovan.infura.io/v3/5e66f831443940ed88e9adca82578c2b"))

def ethsync(request):
    maxblockindb = Ethtxs.objects.aggregate(Max('block'))
    
    endblock = int(web3.eth.blockNumber)
    
    if maxblockindb['block__max'] is None:
        maxblockindb = endblock - 20000


    print('Current best block in index: ' + str(maxblockindb) + '; in Ethereum chain: ' + str(endblock))

    for block in range(maxblockindb + 1, endblock):
        transactions = web3.eth.getBlockTransactionCount(block)
        if transactions > 0:
            insertion(block, transactions)
        else:
            print('Block ' + str(block) + ' does not contain transactions')

    return JsonResponse(True, status=201)

def ethresp(request):
    hash_add = request.GET['and']

    resp = Ethtxs.objects.filter(Q(txfrom=hash_add) | Q(txto=hash_add))

    lt = []
    for item in resp:
        resp_dict = {}
        resp_dict['time'] = item.time
        resp_dict['txto'] = item.txto
        resp_dict['txfrom'] = item.txfrom
        resp_dict['txhash'] = item.txhash
    try:    
        lt.append(resp_dict)
    except Exception as e:
        lt = []


    return JsonResponse(lt, safe=False)



def insertion(blockid, tr):
    time = web3.eth.getBlock(blockid)['timestamp']
    for x in range(0, tr):
        trans = web3.eth.getTransactionByBlock(blockid, x)
        txhash = trans['hash']
        value = trans['value']
        inputinfo = trans['input']
        fr = trans['from']
        to = trans['to']
        
        print("txhash",txhash.hex())

        db_stmnt = Ethtxs.objects.create(time = time, txfrom = fr, txto = to,  block = blockid, txhash = txhash.hex())
        

