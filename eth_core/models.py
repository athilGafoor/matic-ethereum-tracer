from django.db import models

# Create your models here.

class Ethtxs(models.Model):
    time = models.IntegerField(blank=True, null=True)
    txfrom = models.TextField(blank=True, null=True)
    txto = models.TextField(blank=True, null=True)
    block = models.IntegerField(blank=True, null=True)
    txhash = models.TextField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'ethtxs'
