from django.urls import path
from django.conf.urls import include, url 
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
   url(r'^ethdb/', views.ethsync, name = 'ethdb'),
   url(r'^ethresp/', views.ethresp, name = 'ethresp'),
]
